### LSB Image Steganography Tool (LIST) ###
### Author: Luke Wolfe
### Version: 1.0
### Description: A lightweight Python program that uses the LSB steganography method to hide data within a bitmap image.

import os

###FUNCTIONS - START###
##################################################################################################################################################################################

#function for defining what options the user is choosing
def userChoice():

    global extractOrHide
    global fileOrText
    global carrierBitmapImage
    global fileToHide
    
    loop = True
    while loop == True:
        #choose extract or hide
        print('\n########################################')
        print('\n### EXTRACT OR HIDE? ###\n')
        print("Which would you like to do:\n")
        print("\t1. Hide a message or file within a bitmap image.\n")
        print("\t2. Extract a hidden message or file from an existing bitmap carrier image.\n")
        extractOrHide = input("\nPlease type either \"1\" or \"2\" to make a selection from the list above: ")       
        if str(extractOrHide) in ["1", "2"]:
            loop = False
        else:
            print("\nThe choice selected was not valid. Please enter either \"1\" or \"2\" to continue.")
            print("\n----------------------------------------------------------------------------------")
    
    print('\n########################################')
    
    loop = True
    if extractOrHide == "1":
        while loop == True:
            #choose to hide file or text
            print('\n### HIDE -> FILE OR TEXT? ###\n')
            print("Which would you like to do:\n\n")
            print("\t1. Choose a file to embed in the carrier image.\n")
            print("\t2. Type out a text message to be hidden within the carrier image.\n")
            fileOrText = input("\nPlease type either \"1\" or \"2\" to make a selection from the list above: ")
            
            if fileOrText in ["1", "2"]:
                loop = False
            else:
                print("\nThe choice selected was not valid. Please enter either \"1\" or \"2\" to continue.")
                print("\n----------------------------------------------------------------------------------")
        
        loop = True
        while loop == True:
            print('\n########################################')
            print('\n### CHOOSE CARRIER BITMAP ###\n')
            carrierBitmapImage = input("Choose the carrier bitmap image to hide the message in (must be in current working directory and contain the file extension): ")
            carrierBitmapImage = (currentWorkingDirectory + '\\' + carrierBitmapImage)
            if os.path.isfile(carrierBitmapImage) == True:
                loop = False
            else:
                print('\nWARNING!!! Oops! Looks like this file does not exist. Please try again.')
        
        print('\n########################################')
        
        print('\nNOTE: The maximum number of bytes for the hidden file or characters for the hidden text is ' + str(availableStegSpace(carrierBitmapImage)) + '.\n')
        loop = True
        while loop == True:
            if fileOrText == "1":
                print('### HIDE -> FILE -> CHOOSE FILE TO HIDE ###\n')
                fileToHide = input("Choose the file to embed (must be in current working directory, contain the file extension, and  be smaller than or equal to the size specified in the max file size): ")
                fileToHideFullPath = (currentWorkingDirectory + '\\' + fileToHide)
                insideLoop = True
                if os.path.isfile(fileToHideFullPath) == True:
                    insideLoop = False
                else:
                    print('\nWARNING!!! Oops! Looks like this file does not exist. Please try again.')
                fileOrMessageToBits(fileToHideFullPath)
            elif fileOrText == "2":
                print('### HIDE -> FILE -> TYPE MESSAGE TO HIDE ###\n')
                textToHide = input("Type the message to be hidden here and press enter when done(must be smaller than or equal to the size specified in the max file size): ")
                fileOrMessageToBits(textToHide)
            else:
                print("Oops! Looks like something went wrong! (error - userChoice() - 01)")    
    elif extractOrHide == "2":
        imageToExtractMessageFrom = input("Choose the bitmap image that contains the hidden message (must be in current working directory and contain the file extension): " )
        imageToExtractMessageFrom = (currentWorkingDirectory + '\\' + imageToExtractMessageFrom)
        originalImage = input("Choose the original bitmap image used to hide the message (must be in current working directory and contain the file extension): ")
        originalImage = (currentWorkingDirectory + '\\' + originalImage)
   
def endianConversion(bytesToConvert):
    return bytesToConvert[::-1]

def getFileBytesRange(file, firstByte='', lastByte=''):
    
    bytesArray = []
    
    with open(file, 'rb') as bitmapFile:
        data = bitmapFile.read()
        bitmapFile.close()
    for i in range(firstByte,(lastByte+1)):
        j=i*2
        bytesArray.append(data.hex()[j:(j+2)])
    return bytesArray

def availableStegSpace(carrierImage):
    
    global availableFileSpace
    global offset
    global availableFileSpace
    
    offset = ''
    offset = int(offset.join(endianConversion(getFileBytesRange(carrierBitmapImage, 10, 13))),16)
    imageSize = ''
    imageSize = int(imageSize.join(endianConversion(getFileBytesRange(carrierBitmapImage, 2, 5))),16)
    availableFileSpace = int((imageSize - offset)/8) #bytes or characters that can be stored
    return availableFileSpace

def insertData(originalCarrier, hiddenMessage):
    
    global encodedText
    encodedText = originalCarrier[:(offset+1)]
    
    with open(originalCarrier, 'rb') as originalBitmapImage:
        originalBitmapImageData = originalBitmapImage.read()
        originalBitmapImage.close()
    
    for m in range(len(messageData)):
        imageByte=((m*2)+offset)
        if originalBitmapImageData.hex()[imageByte:(imageByte+2)] in ["FF", "ff"]:
            if messageData[m] in ["1", 1]
                temp = int(originalBitmapImageData.hex()[imageByte:(imageByte+2)], 16) + 1

def getOS():
    
    global currentWorkingDirectory
    
    if os.name == 'nt':
        currentWorkingDirectory = os.path.dirname(__file__)
    else:
        currentWorkingDirectory = os.getcwd()

def fileOrMessageToBits(fileOrMessage):
    
    global fileOrMessageArray
    global messageData
    
    fileOrMessageArray = []
    
    if fileOrText == '1':
        with open(fileOrMessage, 'rb') as toHide:
            messageData = toHide.read()
            toHide.close
        messageData = bin(int(messageData.hex(), 16))[2:]
    elif fileOrText == '2':
        messageData = fileOrMessage
        messageData = bin(int(messageData.encode().hex(), 16))[2:]
    else:
        print("Oops! Looks like something went wrong! (error - fileOrMessageToBits() - 01)")
    if len(messageData)%8 == 0:
        pass
    else:
        i = 8 - (len(messageData)%8)
        for x in range(i):
            messageData = '0' + messageData

##################################################################################################################################################################################
###FUNCTIONS - END###

###MAIN - START###
##################################################################################################################################################################################

print('####################################')
print('####################################')
print('##                                ##')
print('##  ██╗     ██╗███████╗████████╗  ##')
print('##  ██║     ██║██╔════╝╚══██╔══╝  ##')
print('##  ██║     ██║███████╗   ██║     ##')
print('##  ██║     ██║╚════██║   ██║     ##')
print('##  ███████╗██║███████║   ██║     ##')
print('##  ╚══════╝╚═╝╚══════╝   ╚═╝     ##')
print('##                                ##')
print('##  LSB Image Steganography Tool  ##')
print('##                                ##')
print('##          -Luke Wolfe-          ##')
print('##                                ##')
print('####################################')
print('####################################')

getOS()

main = True
while main == True:
    userChoice()
    

    
##################################################################################################################################################################################
###MAIN - END###
